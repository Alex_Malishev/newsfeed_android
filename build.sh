#!/usr/bin/env bash
#/bin/bash
cd ..
GRADLE="./gradlew"
${GRADLE} assembleRelease
RELEASEFILE="${CIRCLE_ARTIFACTS}/alphanews-release-${CIRCLE_BUILD_NUM}-${CIRCLE_SHA1}.apk"
cp ./app/release/app-release.apk ${RELEASEFILE}