package ru.aron_stoun.alphabanknews.data.local.news

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import ru.aron_stoun.alphabanknews.data.model.db.FavNewsDB
import ru.aron_stoun.alphabanknews.data.model.db.NewsDB

@Database(entities = arrayOf(NewsDB::class, FavNewsDB::class), version = 2)
abstract class NewsFeedDatabase : RoomDatabase() {

    abstract fun newsfeedDao(): NewsFeedDao

}