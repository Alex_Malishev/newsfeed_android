package ru.aron_stoun.alphabanknews.data.remote

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET
import ru.aron_stoun.alphabanknews.data.model.ChannelResponse
import ru.aron_stoun.alphabanknews.data.model.RssResponse

interface NewsRemoteSource {

    @GET("_/rss/_rss.html?subtype=1&category=2&city=21")
    fun getNews(): Single<RssResponse>

    @GET("_/rss/_rss.html?subtype=1&category=2&city=21")
    fun getNewsSync(): Call<RssResponse>
}