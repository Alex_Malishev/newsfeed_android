package ru.aron_stoun.alphabanknews.data.model.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "fav_news", foreignKeys = arrayOf(ForeignKey(
        entity = NewsDB::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("news_id"),
        onDelete = ForeignKey.CASCADE
)))
data class FavNewsDB(
        @ColumnInfo(name = "news_id") var newsId:Long?
) {
   @PrimaryKey(autoGenerate = true) var id:Long? = null
}