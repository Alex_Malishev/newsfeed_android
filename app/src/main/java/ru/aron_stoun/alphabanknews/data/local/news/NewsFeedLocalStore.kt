package ru.aron_stoun.alphabanknews.data.local.news

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import ru.aron_stoun.alphabanknews.data.model.db.FavNewsDB
import ru.aron_stoun.alphabanknews.data.model.db.NewsDB
import javax.inject.Inject

class NewsFeedLocalStore @Inject constructor(val newsFeedDatabase: NewsFeedDatabase) {


    fun saveNews(newsList: List<NewsDB>) {
        val list = newsFeedDatabase.newsfeedDao().getAllNews()

        val o2 = Observable.fromIterable(newsList)


        val result = o2.concatMap {
            t -> Observable.just(list.contains(t))
        }.zipWith(o2, BiFunction { b: Boolean, newsDB: NewsDB ->
            if (b) newsDB.link = null
            return@BiFunction newsDB
        }).filter {
            it.link != null
        }.toList()


        result.subscribeBy( onError = {
            print(it)
        },
                onSuccess = {
            newsFeedDatabase.newsfeedDao().insertNews(it)
        })


    }

    fun getNews(): Single<List<NewsDB>> {
        return Observable.fromCallable{newsFeedDatabase.newsfeedDao().getAllNews()}
                .flatMap { Observable.fromIterable(it) }
                .map {
                    val count = newsFeedDatabase.newsfeedDao().findNewsInFavs(it.id!!)
                    it.isFavourite = count > 0
                    return@map it
                }.toList()

    }

    fun getFavNews(): Single<List<NewsDB>>{
        return Observable.fromCallable{newsFeedDatabase.newsfeedDao().getFavNews()}
                .flatMap { Observable.fromIterable(it)}
                .map { it.news }.toList()
    }

    fun addNewsToFav(newsId: Long):Completable{
        return Completable.fromCallable{
            val favNewsDB = FavNewsDB(newsId)
            newsFeedDatabase.newsfeedDao().insertFavNews(favNewsDB)
        }
    }

    fun removeNewsFromFav(favId: Long): Completable{
        return Completable.fromCallable{
            newsFeedDatabase.newsfeedDao().deleteFavNews(favId)
        }
    }
}