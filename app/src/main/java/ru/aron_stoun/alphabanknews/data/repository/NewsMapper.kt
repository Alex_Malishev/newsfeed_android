package ru.aron_stoun.alphabanknews.data.repository

import ru.aron_stoun.alphabanknews.data.model.ChannelResponse
import ru.aron_stoun.alphabanknews.data.model.NewsItem
import ru.aron_stoun.alphabanknews.data.model.db.NewsDB
import ru.aron_stoun.alphabanknews.domain.models.Channel
import ru.aron_stoun.alphabanknews.domain.models.News

class NewsMapper {

    class Response {
        companion object {
            fun mapChannel(channelResponse: ChannelResponse): Channel {
                return Channel(
                        channelResponse.title,
                        channelResponse.description,
                        mapNews(channelResponse.news)
                )
            }

            fun mapNews(newsItemList: List<NewsItem>): List<News> {
                val newsList = ArrayList<News>()
                for (newsItem in newsItemList) {
                    newsList.add(News(
                            0,
                            newsItem.title,
                            newsItem.link,
                            newsItem.description,
                            newsItem.pubDate,
                            false
                    ))
                }
                return newsList
            }
        }
    }

    class FromDB {
        companion object {

            fun unmapNewsList(newsDbList: List<NewsDB>): List<News> {
                val newsList = ArrayList<News>()
                for (newsDb in newsDbList){
                    newsList.add(unmapNews(newsDb))
                }
                return newsList
            }

            fun unmapNews(newsDB: NewsDB): News{
                return News(
                        newsDB.id!!,
                        newsDB.title!!,
                        newsDB.link!!,
                        newsDB.description!!,
                        newsDB.pubDate!!,
                        newsDB.isFavourite
                )
            }
        }
    }

    class ToDB {

        companion object {

            fun mapNews(news: News): NewsDB {
                return NewsDB(
                        news.title,
                        news.link,
                        news.description,
                        news.pubDate
                )
            }

            fun mapNewsList(newsList: List<News>): List<NewsDB>{
                val list = ArrayList<NewsDB>()
                for (news in newsList){
                    list.add(mapNews(news))
                }
                return list
            }
        }
    }

}