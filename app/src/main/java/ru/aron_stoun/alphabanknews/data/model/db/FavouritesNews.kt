package ru.aron_stoun.alphabanknews.data.model.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation

class FavouritesNews(
//        @Embedded val favNews: FavNewsDB,
        @Embedded val news: NewsDB
)