package ru.aron_stoun.alphabanknews.data.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "item", strict = false)
data class NewsItem(

        @field:Element(name = "title", required = false)
        @param:Element(name = "title", required = false)
        val title: String,

        @field:Element(name = "link", required = false)
        @param:Element(name = "link", required = false)
        val link: String,

        @field:Element(name = "description", required = false)
        @param:Element(name = "description", required = false)
        val description: String,

        @field:Element(name = "pubDate", required = false)
        @param:Element(name = "pubDate", required = false)
        val pubDate: String
)