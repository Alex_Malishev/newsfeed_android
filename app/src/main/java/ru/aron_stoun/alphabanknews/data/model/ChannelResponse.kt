package ru.aron_stoun.alphabanknews.data.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "channel", strict = false)
data class ChannelResponse (
        @field:Element(name = "title", required = false)
        @param:Element(name = "title", required = false)
        val title: String,

        @field:Element(name = "description", required = false)
        @param:Element(name = "description", required = false)
        val description: String,

        @field:ElementList(inline = true, required = false)
        @param:ElementList(inline = true, required = false)
        val news: List<NewsItem>
)