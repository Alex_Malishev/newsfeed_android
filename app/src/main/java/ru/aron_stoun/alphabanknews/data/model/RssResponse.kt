package ru.aron_stoun.alphabanknews.data.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "rss", strict = false)
data class RssResponse(
        @field:Element(name = "channel", required = false)
        @param:Element(name = "channel", required = false)
        val channel: ChannelResponse
)
