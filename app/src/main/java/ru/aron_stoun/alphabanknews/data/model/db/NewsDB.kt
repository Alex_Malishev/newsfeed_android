package ru.aron_stoun.alphabanknews.data.model.db

import android.arch.persistence.room.*

@Entity(tableName = "news")
data class NewsDB(
        @ColumnInfo(name = "title") var title: String?,
        @ColumnInfo(name = "link") var link: String?,
        @ColumnInfo(name = "description") var description: String?,
        @ColumnInfo(name = "pubDate") val pubDate: String?
){
    @PrimaryKey(autoGenerate = true) var id: Long? = null
    @Ignore var isFavourite: Boolean = false

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other !is NewsDB) return false

        return this.pubDate!! == other.pubDate && this.link == other.link
    }

    override fun hashCode(): Int {
        var result = link?.hashCode() ?: 0
        result = 31 * result + (pubDate?.hashCode() ?: 0)
        return result
    }
}

/*


 */