package ru.aron_stoun.alphabanknews.data.local.news

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import ru.aron_stoun.alphabanknews.data.model.db.FavNewsDB
import ru.aron_stoun.alphabanknews.data.model.db.FavouritesNews
import ru.aron_stoun.alphabanknews.data.model.db.NewsDB

@Dao
interface NewsFeedDao {


    @Query(value = "SELECT * from news")
    fun getAllNews(): List<NewsDB>


    @Insert(onConflict = REPLACE)
    fun insertNews(newsDB: List<NewsDB>)

    @Query(value = "DELETE from news")
    fun deleteAllNews()

    @Query(value = "SELECT news.*  from fav_news INNER JOIN news ON fav_news.news_id = news.id")
    fun getFavNews(): List<FavouritesNews>

    @Insert(onConflict = REPLACE)
    fun insertFavNews(fawNewsDB: FavNewsDB)

    @Query(value = "DELETE from fav_news WHERE fav_news.news_id = :favNewsId")
    fun deleteFavNews(favNewsId: Long)

    @Query(value = "SELECT COUNT(*) from fav_news WHERE fav_news.news_id = :newsId")
    fun findNewsInFavs(newsId: Long): Int
}