package ru.aron_stoun.alphabanknews.data.repository

import android.util.Log
import io.reactivex.Completable
import io.reactivex.Single
import ru.aron_stoun.alphabanknews.data.local.news.NewsFeedLocalStore
import ru.aron_stoun.alphabanknews.data.remote.NewsRemoteSource
import ru.aron_stoun.alphabanknews.domain.models.Channel
import ru.aron_stoun.alphabanknews.domain.models.News
import ru.aron_stoun.alphabanknews.domain.news.INewsRepository
import ru.aron_stoun.alphabanknews.utils.ServiceGenerator
import javax.inject.Inject


class NewsRepository
@Inject constructor(serviceGenerator: ServiceGenerator, private val newsFeedLocalStore: NewsFeedLocalStore) : INewsRepository {



    private var newsRemoteSource: NewsRemoteSource = serviceGenerator.createService(NewsRemoteSource::class.java)


    override fun getNews(): Single<List<News>> {
        return newsFeedLocalStore.getNews().map {
            return@map NewsMapper.FromDB.unmapNewsList(it)
        }
    }

    private val TAG = NewsRepository::class.simpleName

    override fun fetchNewsSync(): List<News> {
        val response = newsRemoteSource.getNewsSync().execute()
        val rssBody = response.body()
        val channel = if (rssBody != null) NewsMapper.Response.mapChannel(rssBody.channel) else null
        if (channel != null) saveChannelNews(channel)
        Log.e(TAG, "fetching news")
        return channel?.news ?: ArrayList()
    }

    override fun fetchNews(): Single<List<News>> {
        return newsRemoteSource.getNews()
                .flatMap {
                    val channel = NewsMapper.Response.mapChannel(it.channel)
                    saveChannelNews(channel)
                    return@flatMap getNews()
                }
    }

    override fun addToFav(news: News): Completable {
        return newsFeedLocalStore.addNewsToFav(news.localId)
    }

    override fun removeFromFav(news: News): Completable {
        return newsFeedLocalStore.removeNewsFromFav(news.localId)
    }

    override fun getFavNews(): Single<List<News>> {
        return newsFeedLocalStore.getFavNews().map {
            return@map NewsMapper.FromDB.unmapNewsList(it)
        }
    }

    private fun saveChannelNews(channel: Channel){
        newsFeedLocalStore.saveNews(NewsMapper.ToDB.mapNewsList(channel.news))
    }
}