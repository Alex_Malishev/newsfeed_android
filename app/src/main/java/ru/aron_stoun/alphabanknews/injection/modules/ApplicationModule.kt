package ru.aron_stoun.alphabanknews.injection.modules

import android.app.Application
import android.content.Context

import dagger.Module
import dagger.Provides
import ru.aron_stoun.alphabanknews.injection.ApplicationContext

/**
 * Provide application-level dependencies.
 */
@Module
class ApplicationModule(private val mApplication: Application) {

    @Provides
    internal fun provideApplication(): Application {
        return mApplication
    }

    @Provides
    @ApplicationContext
    internal fun provideContext(): Context {
        return mApplication
    }


}
