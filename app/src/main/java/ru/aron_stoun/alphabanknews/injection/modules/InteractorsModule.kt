package ru.aron_stoun.alphabanknews.injection.modules

import dagger.Module
import dagger.Provides
import ru.aron_stoun.alphabanknews.domain.news.INewsInteractor
import ru.aron_stoun.alphabanknews.domain.news.INewsRepository
import ru.aron_stoun.alphabanknews.domain.news.NewsInteractor
import ru.aron_stoun.alphabanknews.domain.news_details.INewsDetailsInteractor
import ru.aron_stoun.alphabanknews.domain.news_details.NewsDetailsInteractor

@Module
class InteractorsModule {

    @Provides
    fun provideNewsInteractor(newsRepository: INewsRepository): INewsInteractor{
        return NewsInteractor(newsRepository)
    }

    @Provides
    fun provideNewsDetailsInteractor(newsRepository: INewsRepository):INewsDetailsInteractor{
        return NewsDetailsInteractor(newsRepository)
    }
}