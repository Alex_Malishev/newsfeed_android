package ru.aron_stoun.alphabanknews.injection.modules


import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import ru.aron_stoun.alphabanknews.utils.ServiceGenerator
import ru.aron_stoun.alphabanknews.utils.UnsafeOkHttpClient

/**
 * Created by jarvi on 21.07.2017.
 */

@Module
class NetModule {

    companion object {
        val API_BASE_URL = "https://alfabank.ru/"
    }

    @Provides
    @Singleton
    internal fun provideBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .client(UnsafeOkHttpClient.getUnsafeOkHttp())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient(): OkHttpClient.Builder {
        return UnsafeOkHttpClient.getUnsafeOkHttp().newBuilder()
    }

    @Provides
    @Singleton
    internal fun provideServiceGenerator(okBuilder : OkHttpClient.Builder, retrofit: Retrofit.Builder):ServiceGenerator{
        return ServiceGenerator(okBuilder, retrofit)
    }

}
