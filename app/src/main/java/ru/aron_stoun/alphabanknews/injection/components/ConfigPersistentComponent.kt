package ru.aron_stoun.alphabanknews.injection.components

import dagger.Component
import ru.aron_stoun.alphabanknews.domain.news.INewsInteractor
import ru.aron_stoun.alphabanknews.presentation.base.BaseActivity
import ru.aron_stoun.alphabanknews.injection.ConfigPersistent
import ru.aron_stoun.alphabanknews.injection.modules.ActivityModule
import ru.aron_stoun.alphabanknews.injection.modules.InteractorsModule


/**
 * A dagger component that will live during the lifecycle of an Activity but it won't
 * be destroy during configuration changes. Check [BaseActivity] to see how this components
 * survives configuration changes.
 * Use the [ConfigPersistent] scope to annotate dependencies that need to survive
 * configuration changes (for example Presenters).
 */

@ConfigPersistent
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(InteractorsModule::class))
interface ConfigPersistentComponent {

    fun activityComponent(activityModule: ActivityModule): ActivityComponent

    fun newsInteractor():INewsInteractor
}