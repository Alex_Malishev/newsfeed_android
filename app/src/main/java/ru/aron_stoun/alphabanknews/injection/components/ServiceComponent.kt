package ru.aron_stoun.alphabanknews.injection.components

import dagger.Component
import ru.aron_stoun.alphabanknews.injection.PerService

@PerService
@Component(dependencies = arrayOf(ApplicationComponent::class))
interface ServiceComponent {

}