package ru.aron_stoun.alphabanknews.injection.modules

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import ru.aron_stoun.alphabanknews.data.local.news.NewsFeedDatabase
import javax.inject.Singleton

@Module
class DatabaseModule(val context: Context) {

    @Provides
    @Singleton
    fun provideNewsFeedDatabase():NewsFeedDatabase{
        return Room.databaseBuilder(context.applicationContext,
                NewsFeedDatabase::class.java, "newsfeed.db")
                .build()
    }
}