package ru.aron_stoun.alphabanknews.injection.components

import dagger.Subcomponent
import ru.aron_stoun.alphabanknews.injection.PerActivity
import ru.aron_stoun.alphabanknews.injection.modules.ActivityModule
import ru.aron_stoun.alphabanknews.presentation.news_details.NewsDetailsActivity
import ru.aron_stoun.alphabanknews.presentation.news_details.NewsDetailsFragment
import ru.aron_stoun.alphabanknews.presentation.newsfeed.NewsFeedActivity
import ru.aron_stoun.alphabanknews.presentation.splash.SplashscreenActivity

@PerActivity
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun inject(newsFeedActivity: NewsFeedActivity)
    fun inject(splashscreenActivity: SplashscreenActivity)
    fun inject(newsDetailsActivity: NewsDetailsActivity)
    fun inject(newsDetailsFragment: NewsDetailsFragment)
}