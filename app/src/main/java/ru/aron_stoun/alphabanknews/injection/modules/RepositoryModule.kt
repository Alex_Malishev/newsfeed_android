package ru.aron_stoun.alphabanknews.injection.modules

import dagger.Module
import dagger.Provides
import ru.aron_stoun.alphabanknews.data.repository.NewsRepository
import ru.aron_stoun.alphabanknews.domain.news.INewsRepository
import javax.inject.Singleton

@Module(includes = arrayOf(NetModule::class, DatabaseModule::class))
class RepositoryModule {

    @Provides
    @Singleton
    fun provideNewsRepository(newsRepository: NewsRepository): INewsRepository{
        return newsRepository
    }
}