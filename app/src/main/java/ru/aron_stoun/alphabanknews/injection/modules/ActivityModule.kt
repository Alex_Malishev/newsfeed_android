package ru.aron_stoun.alphabanknews.injection.modules

import android.app.Activity
import android.content.Context
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity

import dagger.Module
import dagger.Provides
import ru.aron_stoun.alphabanknews.injection.ActivityContext

@Module
class ActivityModule(private val appCompatActivity: AppCompatActivity) {

    @Provides
    internal fun provideActivity(): Activity {
        return appCompatActivity
    }

    @Provides
    @ActivityContext
    internal fun providesContext(): Context {
        return appCompatActivity
    }

    @Provides
    internal fun provideFragmentManager(): FragmentManager {
        return appCompatActivity.supportFragmentManager
    }
}
