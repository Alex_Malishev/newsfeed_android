package ru.aron_stoun.alphabanknews.injection.components

import android.app.Application
import android.content.Context
import dagger.Component
import ru.aron_stoun.alphabanknews.domain.news.INewsRepository
import ru.aron_stoun.alphabanknews.injection.ApplicationContext
import ru.aron_stoun.alphabanknews.injection.modules.ApplicationModule
import ru.aron_stoun.alphabanknews.injection.modules.NetModule
import ru.aron_stoun.alphabanknews.injection.modules.RepositoryModule
import ru.aron_stoun.alphabanknews.presentation.works.NewsWorker
import ru.aron_stoun.alphabanknews.utils.ServiceGenerator
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, RepositoryModule::class))
interface ApplicationComponent {

    @ApplicationContext
    fun context(): Context
    fun application(): Application

    fun newsRepository(): INewsRepository

    fun inject(newsWorker: NewsWorker)
}