package ru.aron_stoun.alphabanknews

import android.app.Application
import android.content.Context
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import ru.aron_stoun.alphabanknews.injection.components.ApplicationComponent
import ru.aron_stoun.alphabanknews.injection.components.DaggerApplicationComponent
import ru.aron_stoun.alphabanknews.injection.modules.ApplicationModule
import ru.aron_stoun.alphabanknews.injection.modules.DatabaseModule
import ru.aron_stoun.alphabanknews.injection.modules.RepositoryModule



class NewsApplication : Application() {

    private var applicationComponent: ApplicationComponent? = null

    companion object {
        lateinit var instance : NewsApplication

        fun get():NewsApplication{
            return instance
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        Fabric.with(this, Crashlytics())
    }

    fun getComponent(): ApplicationComponent{
        if (applicationComponent == null){
            applicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(ApplicationModule(this))
                    .repositoryModule(RepositoryModule())
                    .databaseModule(DatabaseModule(this))
                    .build()
        }
        return applicationComponent as ApplicationComponent
    }



}