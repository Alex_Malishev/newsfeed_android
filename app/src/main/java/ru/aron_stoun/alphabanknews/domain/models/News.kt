package ru.aron_stoun.alphabanknews.domain.models

class News(
        var localId: Long,
        var title: String,
        var link: String,
        var description: String,
        var pubDate: String,
        var isFavourite: Boolean
)
