package ru.aron_stoun.alphabanknews.domain.models

class Channel(
        val title: String,
        val description: String,
        val news: List<News>
)