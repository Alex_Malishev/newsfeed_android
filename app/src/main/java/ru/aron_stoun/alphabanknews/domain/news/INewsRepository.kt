package ru.aron_stoun.alphabanknews.domain.news

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import ru.aron_stoun.alphabanknews.domain.models.News

interface INewsRepository {

    fun getNews(): Single<List<News>>
    fun fetchNewsSync(): List<News>
    fun fetchNews(): Single<List<News>>
    fun getFavNews(): Single<List<News>>
    fun addToFav(news: News):Completable
    fun removeFromFav(news: News):Completable

}