package ru.aron_stoun.alphabanknews.domain.news_details

import io.reactivex.Completable
import io.reactivex.Single
import ru.aron_stoun.alphabanknews.domain.models.News
import ru.aron_stoun.alphabanknews.domain.news.INewsRepository

class NewsDetailsInteractor(private val newsRepository: INewsRepository): INewsDetailsInteractor {


    override fun addToFav(news: News): Completable {
        return newsRepository.addToFav(news)
    }

    override fun removeFromFav(news: News): Completable {
        return newsRepository.removeFromFav(news)
    }

    override fun getNews(): Single<List<News>> {
        return newsRepository.getNews()
    }
}