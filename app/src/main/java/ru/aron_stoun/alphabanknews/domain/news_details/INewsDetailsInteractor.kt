package ru.aron_stoun.alphabanknews.domain.news_details

import io.reactivex.Completable
import io.reactivex.Single
import ru.aron_stoun.alphabanknews.domain.models.News

interface INewsDetailsInteractor {

    fun getNews(): Single<List<News>>
    fun addToFav(news: News): Completable
    fun removeFromFav(news: News): Completable

}