package ru.aron_stoun.alphabanknews.domain.news

import io.reactivex.Completable
import io.reactivex.Single
import ru.aron_stoun.alphabanknews.domain.models.News

interface INewsInteractor {

    fun getNews(): Single<List<News>>
    fun fetchNews(): Single<List<News>>

}