package ru.aron_stoun.alphabanknews.domain.news

import io.reactivex.Completable
import io.reactivex.Single
import ru.aron_stoun.alphabanknews.domain.models.News

class NewsInteractor(private val newsRepository: INewsRepository): INewsInteractor {


    override fun fetchNews(): Single<List<News>> {
        return newsRepository.fetchNews()
    }

    override fun getNews(): Single<List<News>> {
        return newsRepository.getNews()
    }

}