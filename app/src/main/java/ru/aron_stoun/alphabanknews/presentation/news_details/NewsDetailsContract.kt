package ru.aron_stoun.alphabanknews.presentation.news_details

import ru.aron_stoun.alphabanknews.domain.models.News
import ru.aron_stoun.alphabanknews.presentation.base.BaseView

interface NewsDetailsContract {

    interface Presesnter{
        fun getNews(isFiltered: Boolean)
        fun favs(news: News)
    }

    interface MvpView : BaseView{
        fun onNews(newsList: List<News>)
        fun onAddToFavs()
        fun onRemoveToFavs()
    }
}