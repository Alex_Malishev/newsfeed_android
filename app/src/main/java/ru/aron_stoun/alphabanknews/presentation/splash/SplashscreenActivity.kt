package ru.aron_stoun.alphabanknews.presentation.splash

import android.graphics.Color
import android.graphics.ColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import kotlinx.android.synthetic.main.activity_splashscreen.*
import ru.aron_stoun.alphabanknews.R
import ru.aron_stoun.alphabanknews.presentation.newsfeed.NewsFeedActivity
import ru.aron_stoun.alphabanknews.presentation.base.BaseActivity
import ru.aron_stoun.alphabanknews.presentation.works.NewsWorker
import ru.aron_stoun.alphabanknews.utils.SPUtils
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashscreenActivity : BaseActivity(), SplashscreenContract.MvpView {


    @Inject
    lateinit var splashscreenPresenter: SplashscreenPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent().inject(this)
        setContentView(R.layout.activity_splashscreen)

        splashscreenPresenter.attachView(this)

        splashProgressBar.indeterminateDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)

        getNewsWithDelayIfNeeded()
    }

    fun getNewsWithDelayIfNeeded() {


        Handler().postDelayed({
            if (SPUtils.checkFirstRun()) {
                splashProgressBar.visibility = View.VISIBLE
                splashscreenPresenter.getNews()
                SPUtils.disableFirstRun()
            } else {
                splashProgressBar.visibility = View.GONE
                onNewsSaved()
            }
        }, 3000)
    }

    override fun onDestroy() {
        super.onDestroy()
        splashscreenPresenter.detachView()
    }

    override fun onNewsSaved() {
        Log.e("splash", "onNewsSaved")
        splashProgressBar.visibility = View.GONE
        startActivity(NewsFeedActivity.getNewsIntent(this))
        finish()
    }
}
