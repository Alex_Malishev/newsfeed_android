package ru.aron_stoun.alphabanknews.presentation.newsfeed

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import kotlinx.android.synthetic.main.activity_news_feed.*
import kotlinx.android.synthetic.main.app_bar.*
import kotlinx.android.synthetic.main.app_bar_with_spinner.*
import ru.aron_stoun.alphabanknews.R
import ru.aron_stoun.alphabanknews.domain.models.News
import ru.aron_stoun.alphabanknews.presentation.base.BaseActivity
import ru.aron_stoun.alphabanknews.presentation.base.IRecyclerClickListener
import ru.aron_stoun.alphabanknews.presentation.custom.SpinnerToolbar
import ru.aron_stoun.alphabanknews.presentation.news_details.NewsDetailsActivity
import ru.aron_stoun.alphabanknews.presentation.works.NewsWorker
import ru.aron_stoun.alphabanknews.utils.LogUtils
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class NewsFeedActivity : BaseActivity(), NewsfeedContract.MvpView, SwipeRefreshLayout.OnRefreshListener, IRecyclerClickListener, SpinnerToolbar.SpinnerSelectionListener {


    companion object {
        val TAG = NewsFeedActivity::class.java.simpleName!!
        fun getNewsIntent(context: Context): Intent {
            return Intent(context, NewsFeedActivity::class.java)
        }
    }

    
    @Inject
    lateinit var newsRecyclerAdapter: NewsRecyclerAdapter

    @Inject
    lateinit var newsFeedPresenter: NewsFeedPresenter

    private var linearLayoutManager: LinearLayoutManager? = null
    private val LAST_ITEM = "last_item"
    private val CURRENT_FILTER = "current_filter"

    private var lastItemPosition: Int = 0
    private var currentFilter: Int = 0
    private val REQUEST_CHAGES: Int = 2708


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent().inject(this)
        setContentView(R.layout.activity_news_feed)
        setSupportActionBar(spinnerToolbar)

        if (savedInstanceState != null) {
            lastItemPosition = savedInstanceState.getInt(LAST_ITEM)
            currentFilter = savedInstanceState.getInt(CURRENT_FILTER)
        }

        spinnerToolbar.spinnerSelectionListener = this
        spinnerToolbar.currentSpinnerItem = currentFilter
        supportActionBar!!.title = ""


        newsRecyclerView.adapter = newsRecyclerAdapter
        linearLayoutManager = LinearLayoutManager(this)
        newsRecyclerView.layoutManager = linearLayoutManager
        newsRecyclerAdapter.setmRecyclerClickListener(this)
        swipeLayout.setOnRefreshListener(this)

        newsFeedPresenter.attachView(this)
        getNews()

        val work = PeriodicWorkRequest.Builder(NewsWorker::class.java, 10, TimeUnit.MINUTES)
                .build()
        WorkManager.getInstance()!!.enqueue(work)
        WorkManager.getInstance()!!.getStatusById(work.id)
                .observe(this, Observer {
                    if (it != null && it.state.isFinished) {
                        swipeLayout.isRefreshing = true
                        getNews()
                    }
                })

    }

    private fun getNews() {
        newsFeedPresenter.getNews(isFavoursFilter())
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(LAST_ITEM, linearLayoutManager!!.findFirstCompletelyVisibleItemPosition())
        outState.putInt(CURRENT_FILTER, currentFilter)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        if (savedInstanceState != null) {
            lastItemPosition = savedInstanceState.getInt(LAST_ITEM)
            currentFilter = savedInstanceState.getInt(CURRENT_FILTER)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CHAGES && resultCode == Activity.RESULT_OK){
            getNews()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onNews(newsList: List<News>) {
        swipeLayout.isRefreshing = false
        newsRecyclerAdapter.clear()
        newsRecyclerAdapter.addAll(newsList)
        newsRecyclerView.scrollToPosition(lastItemPosition)
    }

    override fun onNewsEmpty() {
        Snackbar.make(findViewById(android.R.id.content), getString(R.string.empty_list_warn), Snackbar.LENGTH_SHORT).show()
    }

    override fun onRefresh() {
        lastItemPosition = 0
        newsFeedPresenter.fetchNews(isFavoursFilter())
    }

    private fun isFavoursFilter(): Boolean {
        return currentFilter == 1
    }


    override fun onItemClick(position: Int, o: Any) {
        lastItemPosition = position
        startActivityForResult(NewsDetailsActivity.getNewsDetailsIntent(this, position, isFavoursFilter()), REQUEST_CHAGES)
    }

    override fun onItemLongClick(position: Int, o: Any): Boolean {
        return false
    }

    override fun onSpinnerItemSelected(position: Int) {
        LogUtils.e(TAG, "spinner item changed:  $position")
        currentFilter = position
        getNews()

    }

    override fun onNetworkConnectionLost() {
        super.onNetworkConnectionLost()
        swipeLayout.isRefreshing = false
    }
}
