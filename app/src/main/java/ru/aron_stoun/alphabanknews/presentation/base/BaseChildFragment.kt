package ru.aron_stoun.alphabanknews.presentation.base

import android.content.IntentFilter
import android.support.v4.app.Fragment

import ru.aron_stoun.alphabanknews.injection.components.ActivityComponent


/**
 * Created by jarvi on 25.08.2017.
 */

open class BaseChildFragment : Fragment(), BaseView {


    val component: ActivityComponent
        get() = (activity as BaseActivity).activityComponent()

    fun setTitle(title: String) {
        if (activity != null) activity!!.title = title
    }

    override fun onProgressStart() {

    }

    override fun onProgressStop() {

    }

    override fun onNetworkConnectionLost() {

    }

    override fun onTokenIsDead() {

    }
}
