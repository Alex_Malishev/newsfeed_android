package ru.aron_stoun.alphabanknews.presentation.base

/**
 * Created by jarvi on 01.07.2017.
 */

open class BasePresenter<T : BaseView> : Presenter<T> {

    private var mView: T? = null

    val isViewAttached: Boolean
        get() = mView != null

    override fun attachView(view: T) {
        mView = view
    }

    override fun detachView() {
        mView = null
    }

    fun getmView(): T? {
        return mView
    }

    fun checkViewAttached() {
        if (!isViewAttached) throw MvpViewNotAttachedException()
    }

    class MvpViewNotAttachedException : RuntimeException("Please call Presenter.attachView(MvpView) before" + " requesting data to the Presenter")
}
