package ru.aron_stoun.alphabanknews.presentation.news_details

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import ru.aron_stoun.alphabanknews.domain.models.News
import ru.aron_stoun.alphabanknews.domain.news_details.INewsDetailsInteractor
import ru.aron_stoun.alphabanknews.presentation.base.BasePresenter
import javax.inject.Inject

class NewsDetailsPresenter @Inject constructor(val newsDetailsInteractor: INewsDetailsInteractor): BasePresenter<NewsDetailsContract.MvpView>(), NewsDetailsContract.Presesnter{




    override fun getNews(isFiltered: Boolean) {
        var ob1 = newsDetailsInteractor.getNews()
        if (isFiltered) {
            ob1 = ob1.flatMap {
                Observable.fromIterable(it)
                        .filter { it.isFavourite }
                        .toList()
            }
        }
        ob1.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribeBy(
                        onSuccess = { if (isViewAttached) getmView()!!.onNews(it) },
                        onError = { print(it) }
                )

    }

    override fun favs(news: News) {
        if (news.isFavourite){
            newsDetailsInteractor.removeFromFav(news)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeBy(
                            onError = { print(it)},
                            onComplete = { if (isViewAttached)getmView()!!.onRemoveToFavs() }
                    )
        }else{
            newsDetailsInteractor.addToFav(news)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribeBy(
                            onError = { print(it)},
                            onComplete = { if (isViewAttached) getmView()!!.onAddToFavs()}
                    )
        }
    }
}