package ru.aron_stoun.alphabanknews.presentation.base

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import java.util.HashMap
import java.util.concurrent.atomic.AtomicLong

import ru.aron_stoun.alphabanknews.NewsApplication
import ru.aron_stoun.alphabanknews.injection.components.ActivityComponent
import ru.aron_stoun.alphabanknews.injection.components.ConfigPersistentComponent
import ru.aron_stoun.alphabanknews.injection.components.DaggerConfigPersistentComponent
import ru.aron_stoun.alphabanknews.injection.modules.ActivityModule


/**
 * Abstract activity that every other Activity in this application must implement. It handles
 * creation of Dagger components and makes sure that instances of ConfigPersistentComponent survive
 * across configuration changes.
 */
@SuppressLint("Registered")
abstract class BaseActivity : AppCompatActivity(), BaseView {

    private var mActivityComponent: ActivityComponent? = null
    private var mActivityId: Long = 0

    companion object {

        private val KEY_ACTIVITY_ID = "KEY_ACTIVITY_ID"
        private val NEXT_ID = AtomicLong(0)
        private val sComponentsMap = HashMap<Long, ConfigPersistentComponent>()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //         Create the ActivityComponent and reuses cached ConfigPersistentComponent if this is
        //         being called after a configuration change.
        mActivityId = savedInstanceState?.getLong(KEY_ACTIVITY_ID) ?: NEXT_ID.getAndIncrement()
        val configPersistentComponent: ConfigPersistentComponent?
        if (!sComponentsMap.containsKey(mActivityId)) {
            configPersistentComponent = DaggerConfigPersistentComponent.builder()
                    .applicationComponent(NewsApplication.get().getComponent())
                    .build()
            sComponentsMap[mActivityId] = configPersistentComponent
        } else {
            configPersistentComponent = sComponentsMap[mActivityId]
        }
        mActivityComponent = configPersistentComponent!!.activityComponent(ActivityModule(this))

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(KEY_ACTIVITY_ID, mActivityId)
    }

    override fun onDestroy() {
        if (!isChangingConfigurations) {
            sComponentsMap.remove(mActivityId)
        }
        super.onDestroy()
    }

    fun activityComponent(): ActivityComponent {
        return mActivityComponent as ActivityComponent
    }

    override fun onProgressStart() {

    }

    override fun onProgressStop() {

    }

    override fun onNetworkConnectionLost() {}


    override fun onTokenIsDead() {

    }


}
