package ru.aron_stoun.alphabanknews.presentation.news_details

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.view.ViewPager
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_news_details.*
import kotlinx.android.synthetic.main.app_bar.*
import ru.aron_stoun.alphabanknews.R
import ru.aron_stoun.alphabanknews.domain.models.News
import ru.aron_stoun.alphabanknews.presentation.base.BaseActivity
import javax.inject.Inject

class NewsDetailsActivity : BaseActivity(), NewsDetailsContract.MvpView, ViewPager.OnPageChangeListener {

    @Inject
    lateinit var newsPagerAdapter: NewsPagerAdapter
    @Inject
    lateinit var newsDetailsPresenter: NewsDetailsPresenter

    private var currentPosition: Int = 0
    private var all: Int = 0
    private var item: MenuItem? = null
    private var isFav = false
    private var isChanged = false
    private var isFiltered = false


    private val IS_FAV: String? = "is_fav"

    private val IS_CHANGED: String? = "is_changed"


    companion object {

        val CURRENT_POSITION = "current_position"

        val IS_FILTERED = "is_filtered"

        fun getNewsDetailsIntent(context: Context, position: Int, isFiltered: Boolean): Intent {
            val intent = Intent(context, NewsDetailsActivity::class.java)
            intent.putExtra(CURRENT_POSITION, position)
            intent.putExtra(IS_FILTERED, isFiltered)
            return intent
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent().inject(this)
        setContentView(R.layout.activity_news_details)

        if (savedInstanceState != null) {
            isFav = savedInstanceState.getBoolean(IS_FAV)
            isFiltered = savedInstanceState.getBoolean(IS_FILTERED)
            isChanged = savedInstanceState.getBoolean(IS_CHANGED)
            currentPosition = savedInstanceState.getInt(CURRENT_POSITION)
        }

        if (intent != null) {
            currentPosition = intent.getIntExtra(CURRENT_POSITION, 0)
            isFiltered = intent.getBooleanExtra(IS_FILTERED, false)

        }

        setSupportActionBar(toolbar)
        supportActionBar!!.title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        newsViewPager.addOnPageChangeListener(this)
        newsDetailsPresenter.attachView(this)
        newsDetailsPresenter.getNews(isFiltered)
    }


    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(IS_FAV, isFav)
        outState.putBoolean(IS_FILTERED, isFiltered)
        outState.putBoolean(IS_CHANGED, isChanged)
        outState.putInt(CURRENT_POSITION, currentPosition)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            isFav = savedInstanceState.getBoolean(IS_FAV)
            isChanged = savedInstanceState.getBoolean(IS_CHANGED)
            isFiltered = savedInstanceState.getBoolean(IS_FILTERED)
            currentPosition = savedInstanceState.getInt(CURRENT_POSITION)
        }
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.news_details_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        item = menu!!.findItem(R.id.fav_action)
        if (item != null) {
            item!!.setIcon(
                    if (isFav) R.drawable.baseline_favorite_white_24
                    else R.drawable.baseline_favorite_border_white_24
            )
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onDestroy() {
        super.onDestroy()
        newsDetailsPresenter.detachView()
    }

    override fun onNews(newsList: List<News>) {
        newsPagerAdapter.newsList = newsList
        all = newsList.size
        newsViewPager.adapter = newsPagerAdapter

        onPageSelected(currentPosition)
        newsViewPager.setCurrentItem(currentPosition, false)


    }

    override fun onAddToFavs() {
        isFav = true
        newsPagerAdapter.updateCurrentNews(newsViewPager.currentItem, true)
        invalidateOptionsMenu()
    }

    override fun onRemoveToFavs() {
        isFav = false
        newsPagerAdapter.updateCurrentNews(newsViewPager.currentItem, false)
        invalidateOptionsMenu()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            setResult(if (isChanged) Activity.RESULT_OK else Activity.RESULT_CANCELED)
            onBackPressed()
            return true
        }

        if (item.itemId == R.id.fav_action) {
            isChanged = true
            newsDetailsPresenter.favs(newsPagerAdapter.getCurrentNews(newsViewPager.currentItem))
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun updateTitle(position: Int) {
        supportActionBar!!.title = "${position + 1} ${getString(R.string.of)} $all"
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        currentPosition = position
        updateTitle(position)
        isFav = newsPagerAdapter.getCurrentNews(position).isFavourite
        invalidateOptionsMenu()
    }
}
