package ru.aron_stoun.alphabanknews.presentation.splash

import android.util.Log
import io.reactivex.Scheduler
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import ru.aron_stoun.alphabanknews.domain.models.News
import ru.aron_stoun.alphabanknews.domain.news.INewsInteractor
import ru.aron_stoun.alphabanknews.presentation.base.BasePresenter
import ru.aron_stoun.alphabanknews.utils.NoConnectivityException
import javax.inject.Inject

class SplashscreenPresenter @Inject constructor(private val newsInteractor: INewsInteractor) : BasePresenter<SplashscreenContract.MvpView>(),
        SplashscreenContract.Presenter {

    private val TAG = SplashscreenPresenter::class.simpleName

    override fun getNews() {
        newsInteractor.fetchNews()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribeBy(
                        onSuccess = {
                            Log.e(TAG, "list news size is " + it.size)
                            if (isViewAttached) getmView()!!.onNewsSaved()
                        },
                        onError = {
                            it.printStackTrace()
                            if (it is NoConnectivityException && isViewAttached) getmView()!!.onNetworkConnectionLost()
                        }
                )
    }
}
