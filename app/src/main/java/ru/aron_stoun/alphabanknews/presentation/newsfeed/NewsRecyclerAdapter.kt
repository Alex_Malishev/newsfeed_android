package ru.aron_stoun.alphabanknews.presentation.newsfeed

import android.graphics.Color
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.text.Layout.JUSTIFICATION_MODE_INTER_WORD
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.news_recycler_item.view.*
import ru.aron_stoun.alphabanknews.R
import ru.aron_stoun.alphabanknews.domain.models.News
import ru.aron_stoun.alphabanknews.presentation.base.BaseRecyclerAdapter
import ru.aron_stoun.alphabanknews.utils.ViewUtil
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class NewsRecyclerAdapter @Inject constructor() : BaseRecyclerAdapter<NewsRecyclerAdapter.NewsHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.news_recycler_item, parent, false)
        return NewsHolder(itemView)
    }


    override fun onBindViewHolder(holder: NewsHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        val news: News = this.mCollection[position] as News
        holder.setTitle(news.title)
        holder.setPubDate(news.pubDate)
        holder.setFavour(news.isFavourite)
    }

    private fun formatDate(date: String): String{
        val dateFormat = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH)
        val result = dateFormat.parse(date)
        return SimpleDateFormat("HH:mm, dd MMM yyyy", Locale.getDefault()).format(result)
    }


    inner class NewsHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        var minHeight: Int = ViewUtil.dpToPx(200)
        var maxHeight: Int = 0


        fun setTitle(title: String) {
            itemView.title.text = title
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                itemView.title.justificationMode = JUSTIFICATION_MODE_INTER_WORD
            }
        }

        fun setPubDate(pubDate: String){
            itemView.pubDate.text = formatDate(pubDate)
        }

        fun setFavour(isFav: Boolean){
            itemView.favIcon.visibility = if (isFav) View.VISIBLE else View.GONE
        }
    }
}