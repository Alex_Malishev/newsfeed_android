package ru.aron_stoun.alphabanknews.presentation.works

import androidx.work.Worker
import ru.aron_stoun.alphabanknews.NewsApplication
import ru.aron_stoun.alphabanknews.domain.models.News
import ru.aron_stoun.alphabanknews.domain.news.INewsRepository
import javax.inject.Inject

class NewsWorker : Worker() {

    companion object {
        val NAME = "ru.aronstoun.alphabanknews.NewsWorker"
    }

    @Inject
    lateinit var newsRepository: INewsRepository

    init {
        NewsApplication.get().getComponent().inject(this)
    }

    override fun doWork(): Result {
        val news: List<News>
        return try {
            news = newsRepository.fetchNewsSync()
            if (news.isEmpty()) Result.FAILURE else Result.SUCCESS
        } catch (ex: Exception) {
            Result.FAILURE
        }

    }
}