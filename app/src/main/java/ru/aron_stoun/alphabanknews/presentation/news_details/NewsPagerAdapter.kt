package ru.aron_stoun.alphabanknews.presentation.news_details

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import ru.aron_stoun.alphabanknews.domain.models.News
import javax.inject.Inject

class NewsPagerAdapter @Inject constructor(fragmentManager: FragmentManager):
        FragmentStatePagerAdapter(fragmentManager)  {

    var newsList: List<News>? = null
    var currentPosition: Int = 0

    override fun getItem(position: Int): Fragment {
        return NewsDetailsFragment.newInstance(newsList!![position].link)
    }

    override fun getCount(): Int {
        return newsList!!.size
    }

    fun getCurrentNews(currentPosition: Int):News{
        return newsList!![currentPosition]
    }


    fun updateCurrentNews(currentPosition: Int, isFav: Boolean){
        val news = getCurrentNews(currentPosition)
        news.isFavourite = isFav
    }
}