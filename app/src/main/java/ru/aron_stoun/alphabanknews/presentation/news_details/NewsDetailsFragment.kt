package ru.aron_stoun.alphabanknews.presentation.news_details

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.news_detail_fragment.*
import kotlinx.android.synthetic.main.news_detail_fragment.view.*
import ru.aron_stoun.alphabanknews.R
import ru.aron_stoun.alphabanknews.presentation.base.BaseChildFragment

class NewsDetailsFragment : BaseChildFragment() {

    companion object {

        val LINK = "link"

        fun newInstance(link: String): NewsDetailsFragment {
            val newsDetailsFragment = NewsDetailsFragment()
            val bundle = Bundle()
            bundle.putString(LINK, link)
            newsDetailsFragment.arguments = bundle
            return newsDetailsFragment
        }
    }

    var link: String? = null


    interface NewsDetailsIteraction{
        fun onPageAppear()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            link = arguments!!.getString(LINK)
        }
    }


    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.news_detail_fragment, container, false)

        rootView.webView.settings.javaScriptEnabled = true
        rootView.webView.settings.userAgentString = "Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en)" +
                " AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3"
        rootView.webView.settings.setSupportZoom(true)
        rootView.webView.webViewClient = ProgressWebviewClient()
        rootView.webView.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        rootView.webView.settings.loadWithOverviewMode = true
        rootView.webView.settings.useWideViewPort = true
        rootView.webView.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
        rootView.webView.settings.domStorageEnabled = true

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // chromium, enable hardware acceleration
            rootView.webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        } else {
            // older android version, disable hardware acceleration
            rootView.webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }
        rootView.webView.loadUrl(link)

        return rootView
    }



    inner class ProgressWebviewClient : WebViewClient() {

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            if (progressBar != null) progressBar.visibility = View.VISIBLE
            super.onPageStarted(view, url, favicon)
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            if (progressBar != null) progressBar.visibility = View.GONE
            super.onPageFinished(view, url)
        }
    }
}