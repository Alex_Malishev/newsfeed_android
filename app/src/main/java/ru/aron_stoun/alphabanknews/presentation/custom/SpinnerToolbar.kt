package ru.aron_stoun.alphabanknews.presentation.custom

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.support.v7.widget.Toolbar
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import ru.aron_stoun.alphabanknews.R
import ru.aron_stoun.alphabanknews.utils.ViewUtil

class SpinnerToolbar : Toolbar, AdapterView.OnItemSelectedListener {


    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    interface SpinnerSelectionListener {
        fun onSpinnerItemSelected(position: Int)
    }

    inner class SpinnerAdapter(context: Context?, resource: Int, private val objects: List<String>?)
        : ArrayAdapter<String>(context, resource, objects) {

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
            return getCustomView(position, true)
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            return getCustomView(position, false)
        }

        private fun getCustomView(position: Int, isDropDown: Boolean): View {
            val item = LayoutInflater.from(context).inflate(R.layout.spinner_toolbar, null) as TextView
            item.text = objects!![position]
            item.setTextColor(if (isDropDown) Color.BLACK else Color.WHITE)
            item.typeface = if (!isDropDown) Typeface.DEFAULT_BOLD else Typeface.DEFAULT
            item.setBackgroundColor(if (isDropDown) Color.WHITE else Color.TRANSPARENT)
            item.minWidth = if (isDropDown) ViewUtil.dpToPx(300) else -1
            return item
        }
    }

    var list: List<String>? = arrayListOf("${context.getString(R.string.all)} ${context.getString(R.string.news)}",
            context.getString(R.string.favours))
        set(strings) {
            updateAdapter(strings)
        }

    lateinit var spinnerAdapter: SpinnerAdapter

    var spinnerSelectionListener: SpinnerSelectionListener? = null
        set(listener) {
            field = listener
            spinner.onItemSelectedListener = this
        }
    val spinner = Spinner(context)

    var currentSpinnerItem: Int = 0
        set(value) {
            field = value
            spinner.setSelection(value)
        }

    init {
        updateAdapter(list)
        removeAllViews()
        addView(spinner)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        (spinner.layoutParams as Toolbar.LayoutParams).leftMargin = ViewUtil.dpToPx(20)

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (spinnerSelectionListener != null) spinnerSelectionListener!!.onSpinnerItemSelected(position)
    }

    fun updateAdapter(strings: List<String>?) {
        spinnerAdapter = SpinnerAdapter(context, R.layout.spinner_toolbar, strings)
        spinner.adapter = spinnerAdapter
    }
}