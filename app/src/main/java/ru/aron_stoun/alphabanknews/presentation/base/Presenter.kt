package ru.aron_stoun.alphabanknews.presentation.base

/**
 * Created by jarvi on 01.07.2017.
 */

interface Presenter<V : BaseView> {

    fun attachView(view: V)

    fun detachView()
}
