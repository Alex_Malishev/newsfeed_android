package ru.aron_stoun.alphabanknews.presentation.base

/**
 * Created by jarvi on 18.09.2017.
 */
interface IRecyclerClickListener {

    fun onItemClick(position: Int, o: Any)
    fun onItemLongClick(position: Int, o: Any): Boolean
}
