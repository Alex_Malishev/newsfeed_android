package ru.aron_stoun.alphabanknews.presentation.base


interface BaseView {

    fun onProgressStart()
    fun onProgressStop()
    fun onNetworkConnectionLost()
    fun onTokenIsDead()
}
