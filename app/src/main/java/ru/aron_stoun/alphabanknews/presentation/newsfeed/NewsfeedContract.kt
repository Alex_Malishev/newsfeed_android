package ru.aron_stoun.alphabanknews.presentation.newsfeed

import ru.aron_stoun.alphabanknews.domain.models.News
import ru.aron_stoun.alphabanknews.presentation.base.BaseView

interface NewsfeedContract {

    interface Presenter {
        fun getNews(isFavours: Boolean)
        fun fetchNews(isFavours: Boolean)
    }

    interface MvpView : BaseView{
        fun onNews(newsList: List<News>)
        fun onNewsEmpty()
    }
}