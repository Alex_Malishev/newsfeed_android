package ru.aron_stoun.alphabanknews.presentation.base

import android.support.v7.widget.RecyclerView
import android.view.View

import java.util.ArrayList

/**
 * Created by jarvi on 18.08.2017.
 */

abstract class BaseRecyclerAdapter<T : RecyclerView.ViewHolder> : RecyclerView.Adapter<T>() {

    private var mRecyclerClickListener: IRecyclerClickListener? = null
    val mCollection = ArrayList<Any>()


    fun addAll(objects: Collection<Any>) {
        mCollection.addAll(objects)
        //        notifyItemRangeInserted(mCollection.size(), objects.size());
        notifyDataSetChanged()
    }

    fun addAllWithoutNotify(objects: Collection<Any>) {
        mCollection.addAll(objects)
    }

    fun clear() {
        mCollection.clear()
        notifyDataSetChanged()
    }

    fun clearWithoutNotify() {
        mCollection.clear()
    }

    fun setmRecyclerClickListener(mRecyclerClickListener: IRecyclerClickListener) {
        this.mRecyclerClickListener = mRecyclerClickListener
    }


    override fun onBindViewHolder(holder: T, position: Int) {
        if (mRecyclerClickListener != null) {
            holder.itemView.setOnClickListener { onItemClick(holder) }

            holder.itemView.setOnLongClickListener { onItemLongClick(holder) }
        }


    }

    open fun onItemClick(holder: T) {
        mRecyclerClickListener!!.onItemClick(holder.adapterPosition, mCollection[holder.adapterPosition])
    }

    fun onItemLongClick(holder: T): Boolean {
        return mRecyclerClickListener!!.onItemLongClick(holder.adapterPosition, mCollection[holder.adapterPosition])

    }

    override fun getItemCount(): Int {
        return mCollection.size
    }


}
