package ru.aron_stoun.alphabanknews.presentation.splash

import ru.aron_stoun.alphabanknews.domain.models.News
import ru.aron_stoun.alphabanknews.presentation.base.BaseView

interface SplashscreenContract {

    interface Presenter{
        fun getNews()
    }

    interface MvpView: BaseView{
        fun onNewsSaved()
    }
}