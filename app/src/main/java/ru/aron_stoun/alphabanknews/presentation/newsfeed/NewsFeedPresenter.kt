package ru.aron_stoun.alphabanknews.presentation.newsfeed

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import ru.aron_stoun.alphabanknews.domain.models.News
import ru.aron_stoun.alphabanknews.domain.news.INewsInteractor
import ru.aron_stoun.alphabanknews.presentation.base.BasePresenter
import ru.aron_stoun.alphabanknews.utils.NoConnectivityException
import javax.inject.Inject

class NewsFeedPresenter @Inject constructor(private val newsInteractor: INewsInteractor)
    : BasePresenter<NewsfeedContract.MvpView>(), NewsfeedContract.Presenter {


    override fun getNews(isFavours: Boolean) {
        var ob1 = newsInteractor.getNews()
        if (isFavours) {
            ob1 = ob1.flatMap {
                Observable.fromIterable(it)
                        .filter { it.isFavourite }
                        .toList()
            }
        }
        ob1.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribeBy(
                        onSuccess = { onSuccess(it) },
                        onError = { print(it) }
                )

    }

    override fun fetchNews(isFavours: Boolean) {
        var ob1 = newsInteractor.fetchNews()

        if (isFavours) {
            ob1 = ob1.flatMap {
                Observable.fromIterable(it)
                        .filter { it.isFavourite }
                        .toList()
            }
        }
        ob1.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribeBy(
                        onSuccess = { onSuccess(it) },
                        onError = {
                            if (it is NoConnectivityException && isViewAttached) {
                                getmView()!!.onNetworkConnectionLost()
                            }
                        }
                )
    }

    private fun onSuccess(newsList: List<News>) {
        if (!isViewAttached) return
        if (newsList.isEmpty()) {
            getmView()!!.onNewsEmpty()
        } else {
            getmView()!!.onNews(newsList)
        }
    }
}