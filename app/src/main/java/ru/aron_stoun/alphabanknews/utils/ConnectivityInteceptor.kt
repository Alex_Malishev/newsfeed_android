package ru.aron_stoun.alphabanknews.utils

import java.io.IOException

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response


/**
 * Created by jarvi on 22.08.2017.
 */

class ConnectivityInteceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

        if (!NetworkUtils.isOnline()) {
            throw NoConnectivityException()
        }
        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }

}
