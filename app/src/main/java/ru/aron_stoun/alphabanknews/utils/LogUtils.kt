package ru.aron_stoun.alphabanknews.utils

import android.util.Log

object LogUtils {

    val IS_DEBUG = true

    fun e(tag: String, message: String) {
        if (IS_DEBUG)Log.e(tag, message)
    }

    fun d(tag: String, message: String) {
        if (IS_DEBUG) Log.d(tag, message)
    }


}