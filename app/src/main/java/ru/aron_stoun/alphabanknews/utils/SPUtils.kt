package ru.aron_stoun.alphabanknews.utils

import ru.aron_stoun.alphabanknews.NewsApplication

class SPUtils {

    companion object {
        private val OTHER_STORAGE = "ru.aron_stoun.alphabanknews.STORAGE"

        private val IS_FIRST_START = "is_first_run"

        fun checkFirstRun(): Boolean {
            val sPref = NewsApplication.get().getSharedPreferences(OTHER_STORAGE, 0)
            return sPref.getBoolean(IS_FIRST_START, true)
        }

        fun disableFirstRun() {
            val sPref = NewsApplication.get().getSharedPreferences(OTHER_STORAGE, 0)
            val editor = sPref.edit()
            editor.clear()
            editor.putBoolean(IS_FIRST_START, false)
            editor.apply()
        }
    }

}