package ru.aron_stoun.alphabanknews.utils

import java.util.concurrent.TimeUnit

import javax.inject.Inject
import javax.inject.Singleton

import okhttp3.OkHttpClient
import retrofit2.Retrofit

/**
 * Created by jarvi on 02.07.2017.
 */
@Singleton
class ServiceGenerator @Inject
constructor(private val httpClient: OkHttpClient.Builder, private val builder: Retrofit.Builder) {

    private var retrofit: Retrofit? = null


    fun <S> createService(serviceClass: Class<S>): S {
        val connectivityInteceptor = ConnectivityInteceptor()

        if (!httpClient.interceptors().contains(connectivityInteceptor)) {
            httpClient.addInterceptor(connectivityInteceptor)

        }

        builder.client(httpClient
                .connectTimeout(10, TimeUnit.SECONDS)
                .build())
        retrofit = builder.build()

        return retrofit!!.create(serviceClass)
    }

}
