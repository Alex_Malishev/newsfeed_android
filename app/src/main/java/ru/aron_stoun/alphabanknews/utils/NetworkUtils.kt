package ru.aron_stoun.alphabanknews.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

import ru.aron_stoun.alphabanknews.NewsApplication

class NetworkUtils {

    companion object {
        fun isOnline(): Boolean{
            val connectivityManager = NewsApplication.get().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = connectivityManager.activeNetworkInfo
            return netInfo != null && netInfo.isConnected
        }
    }

}
