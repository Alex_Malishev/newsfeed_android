package ru.aron_stoun.alphabanknews.utils

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.view.inputmethod.InputMethodManager

import ru.aron_stoun.alphabanknews.NewsApplication

object ViewUtil {


    val statusBarHeight: Int
        get() {
            val resource = NewsApplication.get().resources.getIdentifier("status_bar_height", "dimen", "android")
            var statusBarHeight = 0
            if (resource > 0) {
                statusBarHeight = NewsApplication.get().resources.getDimensionPixelSize(resource)
            }
            return statusBarHeight
        }

    fun pxToDp(px: Float): Float {
        val densityDpi = Resources.getSystem().displayMetrics.densityDpi.toFloat()
        return px / (densityDpi / 160f)
    }

    fun dpToPx(dp: Int): Int {
        val density = Resources.getSystem().displayMetrics.density
        return Math.round(dp * density)
    }

    fun screenWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }

    fun screenHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(activity.window.decorView.windowToken, 0)
    }


}
