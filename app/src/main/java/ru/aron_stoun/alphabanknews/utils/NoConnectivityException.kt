package ru.aron_stoun.alphabanknews.utils

import java.io.IOException

/**
 * Created by jarvi on 22.08.2017.
 */

class NoConnectivityException : IOException() {

    override val message: String?
        get() = "No connectivity exception"
}
